import json
import threading
import string
import random
import Adafruit_DHT
import RPi.GPIO as GPIO
import paho.mqtt.client as mqtt

print('\n')
print('----------------------------------------------------')
print('|                   MQTT SAMPLE 1                  |')
print('----------------------------------------------------')
print('\n')

GPIO.setmode(GPIO.BCM)
sensor = Adafruit_DHT.DHT11  # Set sensor type: Options are DHT11, DHT22 or AM2302
gpio = 17  # Set GPIO sensor is connected to
relay = 18  # Set GPIO Relay is connected to
GPIO.setup(relay, GPIO.OUT)  # Setup Mode of GPIO

DEVICE_ID = 'deviceId'  # must change to your deviceId
CLIENT_ID = 'clientId'  # must change to your clientId
MQTT_SERVER = 'iot-mqtt.pod.ir'  # Podiot Mqtt Broker Address
MQTT_PORT = int('1883')  # Podiot Mqtt Broker Port (1883)

# Change this appropiately
SENSOR_NAME_1 = 'temperature'
SENSOR_NAME_2 = 'humidity'
Request_ID = '$requestId'

# publish topics
publishTopicReported = 'dvcasy/twin/update/reported'
publishTopicGet = 'dvcasy/twin/get'
publishTopicDelete = 'dvcasy/twin/delete'

# subscribe topics
subscribeTopicOrigin = 'dvcout/' + DEVICE_ID + '/' + CLIENT_ID + '/#'
subscribeTopicDesired = 'dvcout/' + DEVICE_ID + '/' + CLIENT_ID + '/twin/update/desired'
subscribeTopicDocument = 'dvcout/' + DEVICE_ID + '/' + CLIENT_ID + '/twin/update/document'
subscribeTopicAccepted = 'dvcout/' + DEVICE_ID + '/' + CLIENT_ID + '/twin/response/accepted'
subscribeTopicRejected = 'dvcout/' + DEVICE_ID + '/' + CLIENT_ID + '/twin/response/rejected'


class Payload(object):
    def __init__(self, msg):
        self.__dict__ = json.loads(msg)


# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    print("Connected to Podiot with result code " + str(rc))
    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    client.subscribe(subscribeTopicOrigin)
    # if connection successful start publishing data
    if rc == 0:
        publishTempData()


# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
    json_msg = Payload(msg.payload)
    content = json_msg.content
    main_msg = json.loads(content)
    json_object = json.loads(str(msg.payload.decode()))
    json_object['content'] = json.loads(str(json_object['content']))
    json_formatted_str = json.dumps(json_object, indent=2)
    if msg.topic == subscribeTopicDesired:
        print("\nMessage Received from DesiredTopic: " + json_formatted_str)
        command = main_msg['deviceTwinDocument']['attributes']['desired']['airCondition']
        print(f'command is : {command}')
        if command == 'on':
            GPIO.output(relay, GPIO.HIGH)
        if command == 'off':
            GPIO.output(relay, GPIO.LOW)
    if msg.topic == subscribeTopicDocument:
        print("\nMessage Received from DocumentTopic: " + json_formatted_str)
    if msg.topic == subscribeTopicAccepted:
        print("\nMessage Received from AcceptedTopic: " + json_formatted_str)
        if main_msg['$requestId'] == requestId:
            print('\n')
            print(f'$requestId is  equal {requestId}')
            print('\n')
        else:
            print('\n')
            print('$requestId is not equale')
            print(f'received $requestId is {requestId}')
            print('\n')
    if msg.topic == subscribeTopicRejected:
        print("\nMessage Received from RejectedTopic: " + json_formatted_str)
        if main_msg['$requestId'] == requestId:
            print('\n')
            print(f'$requestId is  equal {requestId}')
            print('\n')
        else:
            print('\n')
            print('$requestId is not equale')
            print(f'received $requestId is {requestId}')
            print('\n')


# Publish random temp data between 10-100
def publishData():
    # Use read_retry method. This will retry up to 15 times to
    # get a sensor reading (waiting 2 seconds between each retry).
    humidity, temperature = Adafruit_DHT.read_retry(sensor, gpio)
    global requestId
    requestId = ''.join(random.choices(string.ascii_uppercase + string.digits, k=10))
    message = json.dumps({Request_ID: requestId, "deviceTwinDocument": {"attributes": {"reported": {SENSOR_NAME_1: str(temperature), SENSOR_NAME_2: str(humidity)}}}},
                         indent=2)
    print("Publish Data: ", message)
    client.publish(publishTopicReported, message)


def publishTempData():
    publishData()
    threading.Timer(60, publishTempData).start()


client = mqtt.Client(CLIENT_ID)
client.on_connect = on_connect
client.on_message = on_message
client.connect(MQTT_SERVER, MQTT_PORT, 60, bind_address="")
client.loop_forever()
